<?php

// creo un objeto de tipo PDO (su clase es PDO)
// la variable conexion me permite comunicarme con 
// el servidor de bbdd
$conexion=new PDO("mysql:host=127.0.0.1;dbname=usuarios","root","");

// utilizo el metodo prepare de un objeto PDO
// para ejecutar una consulta SQL
// la variable respuesta es la que tiene la consulta preparada
$respuesta = $conexion->prepare("select * from usuarios");

// ejecuto la consulta a traves del metodo execute
$respuesta->execute();


// en la variable usuarios tengo todos los registros
// en formato array mixto
// $usuarios[0][1] ==> leer el nombre del primer registro como enumerado
// $usuarios[0]["nombre"] ==> leer el nombre del primer registro como asociativo
$usuarios=$respuesta->fetchAll();

// me crea un texto en formato JSON
// con todos los registros
$usuariosJson=json_encode($usuarios) ;


echo $usuariosJson;


